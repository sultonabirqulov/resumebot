from django.db import models

# Create your models here.
from django.contrib.auth.models import AbstractUser

# Create your models here
# .

LANG_CHOICES = (
    ('uz', "O'zbek"),
    ('ru', "Русский"),
)


class User(AbstractUser):
    chat_id = models.IntegerField(default=0)
    language = models.CharField(choices=LANG_CHOICES, max_length=50, null=True, blank=True)
    phone = models.CharField(max_length=50, null=True, blank=True)

    def __str__(self):
        return self.username