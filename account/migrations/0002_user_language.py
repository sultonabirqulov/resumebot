# Generated by Django 4.0.3 on 2022-03-05 11:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='language',
            field=models.CharField(blank=True, choices=[('uz', "O'zbek"), ('ru', 'Русский'), ('en', 'English')], max_length=50, null=True),
        ),
    ]
