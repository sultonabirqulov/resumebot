from telegram import KeyboardButton, ReplyKeyboardMarkup, InlineKeyboardButton, InlineKeyboardMarkup
from bot.models import Resume
from account.models import User


def home_menu(lan):
    # clear_home(update, callback, activity)
    # activity.type = 'home'
    keyboard = [
        [KeyboardButton(lan['jobs'])],
        [KeyboardButton(lan['social_media']), KeyboardButton(lan['about_us'])],
        [KeyboardButton(lan['edit_language'])],
    ]
    reply_markup = ReplyKeyboardMarkup(keyboard, resize_keyboard=True)
    return reply_markup


def language_menu(lan):
    keyboard = [
        [KeyboardButton("🇺🇿 UZ"), KeyboardButton("🇷🇺 RU")],
    ]
    reply_markup = ReplyKeyboardMarkup(keyboard, resize_keyboard=True)
    return reply_markup


def send_contact_keyboard(lan):
    reply_keyboard = [[KeyboardButton(text=lan['send_phone'], request_contact=True)]]
    reply_markup = ReplyKeyboardMarkup(reply_keyboard, resize_keyboard=True)
    return reply_markup


def social_media(lan):
    keyboard = [
        [KeyboardButton(lan['instagram']), KeyboardButton(lan['facebook'])],
        [KeyboardButton(lan['grandpharm_website'])],
        [KeyboardButton(lan['back'])],

    ]
    print('instagram')

    reply_markup = ReplyKeyboardMarkup(keyboard, resize_keyboard=True)
    return reply_markup


def about_us(lan):
    keyboard = [
        [KeyboardButton(lan['information_about_us'])],
        [KeyboardButton(lan['back'])]
    ]
    reply_markup = ReplyKeyboardMarkup(keyboard, resize_keyboard=True)
    return reply_markup


def jobs_blank(lan):
    keyboard = [
        [KeyboardButton(lan['blank_fill'])],
        [KeyboardButton(lan['back'])]
    ]
    reply_markup = ReplyKeyboardMarkup(keyboard, resize_keyboard=True, one_time_keyboard=True)
    return reply_markup


def about_vakansi(lan):
    keyboard = [
        [KeyboardButton(lan['network']), KeyboardButton(lan['rabotauz'])],
        [KeyboardButton(lan['elon']), KeyboardButton(lan['friends'])]
    ]
    reply_markup = ReplyKeyboardMarkup(keyboard, resize_keyboard=True)
    return reply_markup


def work_friend(lan):
    keyboard = [
        [KeyboardButton(lan['yes']), KeyboardButton(lan['no'])],
    ]
    reply_markup = ReplyKeyboardMarkup(keyboard, resize_keyboard=True)
    return reply_markup


def job_interes(lan):
    keyboard = [
        [KeyboardButton(lan['menejer'])],
        [KeyboardButton(lan['pharmacist']), KeyboardButton(lan['stajor'])]
    ]
    reply_markup = ReplyKeyboardMarkup(keyboard, resize_keyboard=True)
    return reply_markup


def work_time(lan):
    keyboard = [
        [KeyboardButton(lan['morning']), KeyboardButton(lan['lunch'])],
        [KeyboardButton(lan['evening'])]
    ]
    reply_markup = ReplyKeyboardMarkup(keyboard, resize_keyboard=True, one_time_keyboard=True)
    return reply_markup


def education(lan):
    keyboard = [
        [KeyboardButton(lan['high']), KeyboardButton(lan['mediumspecial'])]
    ]
    reply_markup = ReplyKeyboardMarkup(keyboard, resize_keyboard=True, one_time_keyboard=True)
    return reply_markup


def russian_language(lan):
    keyboard = [
        [KeyboardButton(lan['elementary'])],
        [KeyboardButton(lan['middle']), KeyboardButton(lan['senior'])]
    ]
    reply_markup = ReplyKeyboardMarkup(keyboard, resize_keyboard=True)
    return reply_markup


def student(lan):
    keyboard = [
        [KeyboardButton(lan['yes']), KeyboardButton(lan['no'])],
    ]
    reply_markup = ReplyKeyboardMarkup(keyboard, resize_keyboard=True)
    return reply_markup


def work_experience(lan):
    keyboard = [
        [KeyboardButton(lan['no_work']), KeyboardButton(lan['1-year'])],
        [KeyboardButton(lan['2-5-year']), KeyboardButton(lan['5-year+'])]
    ]
    reply_markup = ReplyKeyboardMarkup(keyboard, resize_keyboard=True, one_time_keyboard=True)
    return reply_markup


def result(update, lan):
    query = update.callback_query
    user = User.objects.get(username=update.message.chat_id)
    resume = Resume.objects.filter(user=user)
    for res in resume:
        reply_text1 = '<b>'+lan['name']+res.first_last_name+'\n\n' + lan['phone_number']+res.phone +\
                      '\n\n'+lan['specialty_user']+ res.specialty + '\n\n' + lan['address_user'] + res.address + '\n\n' + lan['user_work_example'] + res.your_work_experience + '</b>\n\n'
        update.message.reply_text(text=reply_text1, parse_mode='HTML')


def finish(lan):
    keyboard = [
        [KeyboardButton(lan['yes']), KeyboardButton(lan['no'])],
    ]
    reply_markup = ReplyKeyboardMarkup(keyboard, resize_keyboard=True, one_time_keyboard=True)
    return reply_markup


def home(lan):
    keyboard = [
        [KeyboardButton(lan['home_menu'])]
    ]
    reply_markup = ReplyKeyboardMarkup(keyboard, resize_keyboard=True, one_time_keyboard=True)
    return reply_markup
