from .log import log_errors, autorization, setLanguage
from bot.models import Activity, Resume
from .commands import start
from .callback_query import instagram, facebook, grandpharm, information_about
from account.models import User
from .keyboards import result, about_vakansi, work_friend, job_interes, work_time, education, russian_language, student, work_experience, home
import importlib, json
from .utils import (
        get_chat_id_by_update, get_user_by_chat_id,
        get_language)


@autorization
def handler(update, callback, user, activity, lan):
    chat_id = update.message.chat_id
    text = update.message.text
    if activity.type == 'set_phone':
        text_contact(update, callback)
    elif text == lan['edit_language']:
        setLanguage(update, callback, user, activity, flag=True)

    elif text == lan['jobs']:
        activity.type = 'jobs'
        activity.save()
        start(update, callback)

    elif text == lan['social_media']:
        activity.type = 'media'
        activity.save()
        start(update, callback)

    elif text == lan['instagram']:
        reply_text = lan['stiker']
        reply_markup = instagram(update, lan)
        res = update.message.reply_text(text=reply_text, reply_markup=reply_markup)
        activity.type = 'instagram'
        activity.save()

    elif text == lan['facebook']:
        reply_text = lan['stiker']
        reply_markup = facebook(update, lan)
        res = update.message.reply_text(text=reply_text, reply_markup=reply_markup)
        activity.type = 'facebook'
        activity.save()

    elif text == lan['grandpharm_website']:
        reply_text = lan['stiker']
        reply_markup = grandpharm(update, lan)
        res = update.message.reply_text(text=reply_text, reply_markup=reply_markup)
        activity.type = 'grandpharm'
        activity.save()

    elif text == lan['information_about_us']:
        reply_text = lan['stiker']
        reply_markup = information_about(update, lan)
        res = update.message.reply_text(text=reply_text, reply_markup=reply_markup)
        activity.type = 'information_about_us'
        activity.save()

    elif text == lan['about_us']:
        activity.type = 'about'
        activity.save()
        start(update, callback)

    elif text == lan['home_menu']:
        activity.type = 'home'
        activity.save()
        start(update, callback)

    elif text == lan['blank_fill']:
        jarayon(update, callback, user, activity, lan)

    elif activity.type == 'f_l_name':
        user_resume(update, callback, user, activity, lan)

    elif activity.type == 'birthday':
        birthday(update, callback, user, activity, lan)

    elif activity.type == 'about_vacancy_network':
        about_vacancy_network(update, callback, user, activity, lan)

    elif activity.type == 'company_work_friend':
        company_work_friend(update, callback, user, activity, lan)

    elif activity.type == 'job_interested':
        job_interested(update, callback, user, activity, lan)

    elif activity.type == 'work_schedule':
        work_schedule(update, callback, user, activity, lan)

    elif activity.type == 'metro_station':
        metro_station(update, callback, user, activity, lan)

    elif activity.type == 'your_education':
        your_education(update, callback, user, activity, lan)

    elif activity.type == 'specialty':
        specialty(update, callback, user, activity, lan)

    elif activity.type == 'level_russian_language':
        level_russian_language(update, callback, user, activity, lan)

    elif activity.type == 'pupil_student':
        pupil_student(update, callback, user, activity, lan)

    elif activity.type == 'your_work_experience':
        your_work_experience(update, callback, user, activity, lan)

    elif activity.type == 'last_job':
        last_job(update, callback, user, activity, lan)

    elif activity.type == 'minimum_wage':
        minimum_wage(update, callback, user, activity, lan)

    elif activity.type == 'image':
        image(update, callback, user, activity, lan)

    elif activity.type == 'address':
        address(update, callback, user, activity, lan)

    elif text == lan['yes']:
        reply_text = lan['finish_yes']
        reply_markup = home(lan)
        res = update.message.reply_text(text=reply_text, reply_markup=reply_markup)
    elif text == lan['no']:
        reply_text = lan['finish_no']
        res = update.message.reply_text(text=reply_text)
        jarayon(update, callback, user, activity, lan)

    elif text == lan['back']:
        start(update, callback)


@log_errors
def set_language(update, callback):
    chat_id = get_chat_id_by_update(update)
    message_id = update.message.message_id
    text = update.message.text
    text = text.split(' ')[1]
    activity, _ = Activity.objects.get_or_create(chat_id=chat_id)
    user = get_user_by_chat_id(update)
    languages = (('uz', 'UZ'), ('ru', 'RU'))
    lan = 'uz'

    for key, value in enumerate(languages):
        (lowercase, uppercase) = value
        if (uppercase == text):
            lan = lowercase

    if user:
        user.language = lan
        user.save()
        detail = json.loads(activity.detail)
        messages = detail['messages'] if 'messages' in detail else []
        messages.append(message_id)
        detail['messages'] = messages
        activity.detail = json.dumps(detail)
        activity.type = 'set_lang'
        activity.save()

    start(update, callback)


def text_contact(update, callback):
    chat_id = update.message.chat_id
    text = update.message.text
    message = update.message
    message_id = update.message.message_id
    activity, _ = Activity.objects.get_or_create(chat_id=chat_id)
    detail = json.loads(activity.detail)
    messages = detail['messages']
    messages.append(message_id)
    user = get_user_by_chat_id(update)

    text = update.message.text
    if text:
        phone = update.message.text
    else:
        phone = message.contact.phone_number

    if user:
        user.phone = phone
        user.save()
        user = update.message.contact.first_name

        detail = json.loads(activity.detail)
        messages = detail['messages'] if 'messages' in detail else []
        messages.append(message_id)
        detail['messages'] = messages
        activity.detail = json.dumps(detail)
        activity.type = 'home'
        activity.save()
    start(update, callback)


def jarayon(update, callback, user, activity, lan):
    reply_text = lan['f_l_name']
    res = update.message.reply_text(text=reply_text)
    activity.type = 'f_l_name'
    activity.save()


def user_resume(update, callback, user, activity, lan):
    chat_id = update.message.chat_id
    user = User.objects.get(username=chat_id)
    user_text = update.message.text
    resume = Resume.objects.filter(user=user).order_by('-id').first()
    if not resume:
        resume = Resume.objects.create(user=user)
    resume.first_last_name = user_text
    resume.phone = user.phone
    resume.save()
    if user_text == lan['back']:
        activity.type = 'jobs'
        activity.save()
        start(update, callback)
    else:
        reply_text = lan['birthday']
        res = update.message.reply_text(text=reply_text)
        activity.type = 'birthday'
        activity.save()


def birthday(update, callback, user, activity, lan):
    chat_id = update.message.chat_id
    user = User.objects.get(username=chat_id)
    user_text = update.message.text
    resume = Resume.objects.filter(user=user).order_by('-id').first()
    if not resume:
        resume = Resume.objects.create(user=user)
    resume.birthday = user_text
    resume.save()
    if user_text == lan['back']:
        activity.type = 'jobs'
        activity.save()
        start(update, callback)
    else:
        reply_text = lan['about_vacancy_network']
        reply_markup = about_vakansi(lan)
        res = update.message.reply_text(text=reply_text, reply_markup=reply_markup)
        activity.type = 'about_vacancy_network'
        activity.save()


def about_vacancy_network(update, callback, user, activity, lan):
    chat_id = update.message.chat_id
    user = User.objects.get(username=chat_id)
    user_text = update.message.text
    resume = Resume.objects.filter(user=user).order_by('-id').first()
    if not resume:
        resume = Resume.objects.create(user=user)
    resume.about_vacancy_network = user_text
    resume.save()
    if user_text == lan['back']:
        activity.type = 'jobs'
        activity.save()
        start(update, callback)
    else:
        reply_text = lan['company_work_friend']
        reply_markup = work_friend(lan)
        res = update.message.reply_text(text=reply_text, reply_markup=reply_markup)
        activity.type = 'company_work_friend'
        activity.save()


def company_work_friend(update, callback, user, activity, lan):
    chat_id = update.message.chat_id
    user = User.objects.get(username=chat_id)
    user_text = update.message.text
    resume = Resume.objects.filter(user=user).order_by('-id').first()
    if not resume:
        resume = Resume.objects.create(user=user)
    resume.company_work_friend = user_text
    resume.save()
    if user_text == lan['back']:
        activity.type = 'jobs'
        activity.save()
        start(update, callback)
    else:
        reply_text = lan['job_interested']
        reply_markup = job_interes(lan)
        res = update.message.reply_text(text=reply_text, reply_markup=reply_markup)
        activity.type = 'job_interested'
        activity.save()


def job_interested(update, callback, user, activity, lan):
    chat_id = update.message.chat_id
    user = User.objects.get(username=chat_id)
    user_text = update.message.text
    resume = Resume.objects.filter(user=user).order_by('-id').first()
    if not resume:
        resume = Resume.objects.create(user=user)
    resume.job_interested = user_text
    resume.save()
    if user_text == lan['back']:
        activity.type = 'jobs'
        activity.save()
        start(update, callback)
    else:
        reply_text = lan['work_schedule']
        reply_markup = work_time(lan)
        res = update.message.reply_text(text=reply_text, reply_markup=reply_markup)
        activity.type = 'work_schedule'
        activity.save()


def work_schedule(update, callback, user, activity, lan):
    chat_id = update.message.chat_id
    user = User.objects.get(username=chat_id)
    user_text = update.message.text
    resume = Resume.objects.filter(user=user).order_by('-id').first()
    if not resume:
        resume = Resume.objects.create(user=user)
    resume.work_schedule = user_text
    resume.save()
    if user_text == lan['back']:
        activity.type = 'jobs'
        activity.save()
        start(update, callback)
    else:
        reply_text = lan['metro_station']
        res = update.message.reply_text(text=reply_text)
        activity.type = 'metro_station'
        activity.save()


def metro_station(update, callback, user, activity, lan):
    chat_id = update.message.chat_id
    user = User.objects.get(username=chat_id)
    user_text = update.message.text
    resume = Resume.objects.filter(user=user).order_by('-id').first()
    if not resume:
        resume = Resume.objects.create(user=user)
    resume.metro_station = user_text
    resume.save()
    if user_text == lan['back']:
        activity.type = 'jobs'
        activity.save()
        start(update, callback)
    else:
        reply_text = lan['your_education']
        reply_markup = education(lan)
        res = update.message.reply_text(text=reply_text, reply_markup=reply_markup)
        activity.type = 'your_education'
        activity.save()


def your_education(update, callback, user, activity, lan):
    chat_id = update.message.chat_id
    user = User.objects.get(username=chat_id)
    user_text = update.message.text
    resume = Resume.objects.filter(user=user).order_by('-id').first()
    if not resume:
        resume = Resume.objects.create(user=user)
    resume.your_education = user_text
    resume.save()
    if user_text == lan['back']:
        activity.type = 'jobs'
        activity.save()
        start(update, callback)
    else:
        reply_text = lan['specialty']
        res = update.message.reply_text(text=reply_text)
        activity.type = 'specialty'
        activity.save()


def specialty(update, callback, user, activity, lan):
    chat_id = update.message.chat_id
    user = User.objects.get(username=chat_id)
    user_text = update.message.text
    resume = Resume.objects.filter(user=user).order_by('-id').first()
    if not resume:
        resume = Resume.objects.create(user=user)
    resume.specialty = user_text
    resume.save()
    if user_text == lan['back']:
        activity.type = 'jobs'
        activity.save()
        start(update, callback)
    else:
        reply_text = lan['level_russian_language']
        reply_markup = russian_language(lan)
        res = update.message.reply_text(text=reply_text, reply_markup=reply_markup)
        activity.type = 'level_russian_language'
        activity.save()


def level_russian_language(update, callback, user, activity, lan):
    chat_id = update.message.chat_id
    user = User.objects.get(username=chat_id)
    user_text = update.message.text
    resume = Resume.objects.filter(user=user).order_by('-id').first()
    if not resume:
        resume = Resume.objects.create(user=user)
    resume.level_russian_language = user_text
    resume.save()
    if user_text == lan['back']:
        activity.type = 'jobs'
        activity.save()
        start(update, callback)
    else:
        reply_text = lan['pupil_student']
        reply_markup = student(lan)
        res = update.message.reply_text(text=reply_text, reply_markup=reply_markup)
        activity.type = 'pupil_student'
        activity.save()


def pupil_student(update, callback, user, activity, lan):
    chat_id = update.message.chat_id
    user = User.objects.get(username=chat_id)
    user_text = update.message.text
    resume = Resume.objects.filter(user=user).order_by('-id').first()
    if not resume:
        resume = Resume.objects.create(user=user)
    resume.pupil_student = user_text
    resume.save()
    if user_text == lan['back']:
        activity.type = 'jobs'
        activity.save()
        start(update, callback)
    else:
        reply_text = lan['your_work_experience']
        reply_markup = work_experience(lan)
        res = update.message.reply_text(text=reply_text, reply_markup=reply_markup)
        activity.type = 'your_work_experience'
        activity.save()


def your_work_experience(update, callback, user, activity, lan):
    chat_id = update.message.chat_id
    user = User.objects.get(username=chat_id)
    user_text = update.message.text
    resume = Resume.objects.filter(user=user).order_by('-id').first()
    if not resume:
        resume = Resume.objects.create(user=user)
    resume.your_work_experience = user_text
    resume.save()
    if user_text == lan['back']:
        activity.type = 'jobs'
        activity.save()
        start(update, callback)
    else:
        reply_text = lan['last_job']
        res = update.message.reply_text(text=reply_text)
        activity.type = 'last_job'
        activity.save()


def last_job(update, callback, user, activity, lan):
    chat_id = update.message.chat_id
    user = User.objects.get(username=chat_id)
    user_text = update.message.text
    resume = Resume.objects.filter(user=user).order_by('-id').first()
    if not resume:
        resume = Resume.objects.create(user=user)
    resume.last_job = user_text
    resume.save()
    if user_text == lan['back']:
        activity.type = 'jobs'
        activity.save()
        start(update, callback)
    else:
        reply_text = lan['minimum_wage']
        res = update.message.reply_text(text=reply_text)
        activity.type = 'minimum_wage'
        activity.save()


def minimum_wage(update, callback, user, activity, lan):
    chat_id = update.message.chat_id
    user = User.objects.get(username=chat_id)
    user_text = update.message.text
    resume = Resume.objects.filter(user=user).order_by('-id').first()
    if not resume:
        resume = Resume.objects.create(user=user)
    resume.minimum_wage = user_text
    resume.save()
    if user_text == lan['back']:
        activity.type = 'jobs'
        activity.save()
        start(update, callback)
    else:
        reply_text = lan['image']
        res = update.message.reply_text(text=reply_text)
        activity.type = 'image'
        activity.save()


@autorization
def image(update, callback, user, activity, lan):
    chat_id = update.message.chat_id
    user = User.objects.get(username=chat_id)
    user_text = update.message.text

    print('a')
    obj = callback.bot.getFile(update.message.photo[-1].file_id)
    # obj.dowload()
    print('obj', obj)
    resume = Resume.objects.filter(user=user).order_by('-id').first()
    if not resume:
        resume = Resume.objects.create(user=user)
    resume.image = obj.file_path
    resume.save()
    if user_text == lan['back']:
        activity.type = 'jobs'
        activity.save()
        start(update, callback)
    else:
        reply_text = lan['address']
        res = update.message.reply_text(text=reply_text)
        activity.type = 'address'
        activity.save()


def address(update, callback, user, activity, lan):
    chat_id = update.message.chat_id
    user = User.objects.get(username=chat_id)
    user_text = update.message.text
    resume = Resume.objects.filter(user=user).order_by('-id').first()
    if not resume:
        resume = Resume.objects.create(user=user)
    resume.address = user_text
    resume.save()
    if user_text == lan['back']:
        activity.type = 'jobs'
        activity.save()
        start(update, callback)
    else:
        reply_text = lan['result']
        reply_markup = result(update, lan)
        res = update.message.reply_text(text=reply_text, reply_markup=reply_markup)
    activity.type = 'result'
    activity.save()
    start(update, callback)