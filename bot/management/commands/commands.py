from .log import autorization, clear_home
from .keyboards import home_menu, language_menu, send_contact_keyboard, social_media, about_us, jobs_blank, finish
import json
from account.models import User
from bot.models import Resume


@autorization
def start(update, callback, user, activity, lan):
    chat_id = update.message.chat_id
    message_id = update.message.message_id
    # clear_home(update, callback, activity)
    user = User.objects.get(chat_id=chat_id)
    # resume = Resume.objects.get(user=chat_id)
    if user.language is None:
        reply_text = lan['start_msg']
        reply_markup = language_menu(lan)
        res = update.message.reply_text(text=reply_text, reply_markup=reply_markup)
    elif user.phone is None:
        reply_text = lan['phone']
        reply_markup = send_contact_keyboard(lan)
        res = update.message.reply_text(text=reply_text, reply_markup=reply_markup)
        activity.type = 'set_phone'
        activity.save()

    elif activity.type == 'jobs':
        reply_text = lan['jobs_vak']
        reply_markup = jobs_blank(lan)
        res = update.message.reply_text(text=reply_text, reply_markup=reply_markup)
        activity.type = 'home'
        activity.save()

    elif activity.type == 'media':
        reply_text = lan['s_media']
        reply_markup = social_media(lan)
        res = update.message.reply_text(text=reply_text, reply_markup=reply_markup)
        activity.type = 'home'
        activity.save()

    elif activity.type == 'about':
        reply_text = lan['about_text']
        reply_markup = about_us(lan)
        res = update.message.reply_text(text=reply_text, reply_markup=reply_markup)
        activity.type = 'home'
        activity.save()

    elif activity.type == 'result':
        reply_text = lan['finish']
        reply_markup = finish(lan)
        res = update.message.reply_text(text=reply_text, reply_markup=reply_markup)
        activity.type = 'finish'
        activity.save()

    else:
        reply_text = lan['start_msg']
        reply_markup = home_menu(lan)
        res = update.message.reply_text(text=reply_text, reply_markup=reply_markup)

    detail = json.loads(activity.detail)

    messages = []
    messages.append(message_id)
    detail['home_message'] = res.message_id
    detail['messages'] = messages
    activity.detail = json.dumps(detail)
    activity.save()
