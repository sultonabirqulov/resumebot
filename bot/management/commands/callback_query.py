from telegram import KeyboardButton, ReplyKeyboardMarkup, InlineKeyboardButton, InlineKeyboardMarkup
from bot.models import Resume


def instagram(update, lan):
    query = update.callback_query

    reply_text = '<a href="https://www.instagram.com/grandpharm.uz/">'+lan['instagram_link']+'</a>'
    update.message.reply_text(text=reply_text, parse_mode='HTML')


def facebook(update, lan):
    query = update.callback_query
    reply_text = '<a href="https://www.facebook.com/GrandPharm.uz/">'+lan['facebook_link']+'</a>'
    update.message.reply_text(text=reply_text, parse_mode='HTML')


def grandpharm(update, lan):
    query = update.callback_query
    reply_text = '<a href="https://grandpharm.uz/">'+lan['grandpharm_link']+'</a>'
    update.message.reply_text(text=reply_text, parse_mode='HTML')


def information_about(update, lan):
    query = update.callback_query
    reply_text = '<a href="https://grandpharm.uz/">'+lan['information_about']+'</a>'
    update.message.reply_text(text=reply_text, parse_mode='HTML')