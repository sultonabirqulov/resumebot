# Generated by Django 4.0.3 on 2022-03-14 06:24

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('bot', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Resume',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_last_name', models.CharField(max_length=255)),
                ('birthday', models.CharField(blank=True, max_length=255, null=True)),
                ('about_vacancy_network', models.CharField(blank=True, max_length=255, null=True)),
                ('company_work_friend', models.CharField(blank=True, max_length=255, null=True)),
                ('job_interested', models.CharField(blank=True, max_length=255, null=True)),
                ('work_schedule', models.CharField(blank=True, max_length=255, null=True)),
                ('metro_station', models.CharField(blank=True, max_length=255, null=True)),
                ('your_education', models.CharField(blank=True, max_length=255, null=True)),
                ('specialty', models.CharField(blank=True, max_length=255, null=True)),
                ('level_russian_language', models.CharField(blank=True, max_length=255, null=True)),
                ('pupil_student', models.CharField(blank=True, max_length=255, null=True)),
                ('your_work_experience', models.CharField(blank=True, max_length=255, null=True)),
                ('minimum_wage', models.CharField(blank=True, max_length=255, null=True)),
                ('image', models.ImageField(blank=True, null=True, upload_to='static/images/')),
                ('address', models.CharField(blank=True, max_length=255, null=True)),
                ('user', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.DeleteModel(
            name='Region',
        ),
    ]
