words = {
    'lang': 'ru',
    'phone': 'Телефон:',
    'select_lang': "🇷🇺 🏪 Пожалуйста, выберите язык 👇",
    'current_location': '📍 Мое местоположение',
    'home_button': "🏠️ Главное меню",
    'back': '⏪ Назад',
    'start_msg': " 💊 Введите название лекарства",
    'edit_language': "🔄 Изменить язык",
    "select_language": "Выберите язык 👇",
    'add_comment': '✍ Оставить отзыв',
    'search_drug': "🔍 Начать поиск",
    'send_phone': "📱 Отправить номер телефона",

    'about_us': "О нас",
    'jobs': 'Вакансии',
    'social_media': "Социальные сети",
    'instagram': "Instagram",
    'facebook': "Facebook",
    'grandpharm_website': "Сайт grandpharm",


    's_media': "Получайте информацию через социальные сети 👇",
    'about_text': "Узнайте больше о нас 👇",
    'information_about_us': "Информация о нас",
    'jobs_vak': "Просим ответить Вас на несколько вопросов",
    'blank_fill': "Начать процесс",

    'instagram_link': "Перейти в Инстаграм",

    'f_l_name': "Введите свое имя и фамилию?",
    'birthday': "Введите свой день рождения?",
    'about_vacancy_network': "Как вы узнали о вакансии?",
    'company_work_friend': "Есть ли кто из ваших знакомых кто работает в нашей компании?",
    'job_interested': "Какая вакансия вас интересует?",
    'work_schedule': "Какая вакансия вас интересует?",
    'metro_station': "Ближайшая станция метро ?",
    'your_education': "Какое у Вас оброзование ?",
    'specialty': "Какая специальность?",
    'level_russian_language': "Уровень вашего русского языка?",
    'pupil_student': "Являетесь ли Вы учеником или студентом в настоящее время ?",
    'your_work_experience': "Какой у Вас опыт работы?",
    'last_job': "Последнее место работы: Период, название организации, должность?",
    'minimum_wage': "Минимальный уровень зарплаты ?",
    'image': "Введите фотографию ",
    'address': "Введите свой адрес проживания",

    'result': "Yakuni natija 👆",

    # KAK vakansi
    'network': "Internet orqali",
    'rabotauz': "Rabota.uz",
    'elon': "Elon orqali",
    'friends': "Do'stlarim orqali",

    # work_ friend
    'yes': "Ha",
    'no': "Yo'q",

    # job_ interested
    'menejer': "Dorihona Menejeri",
    'pharmacist': "Farmatseft",
    'stajor': "Amalyotchi",

    # work time
    'morning': "08:00-16:00",
    'lunch': "12:00-20:00",
    'evening': "14:00-22:00",

    # level russian language
    'elementary': "Boshlang'ich",
    'middle': "O'rta",
    'senior': "O'ziniki",

    # work_experience
    'no_work': "Ish tajribasi yo'q",
    '1-year': "1 yil",
    '2-5-year': "2-5 yil",
    '5-year+': "5 yildan yuqori",

    # finish
    'finish': "Barcha malumotlar to'g'rimi",
    'finish_yes': "Jarayon muofaqqiyatli yakunlandi. Raxmat",
    'finish_no': "Jarayonda xatolik yuz berdi. Qaytatdan urinib ko'ring",

}
