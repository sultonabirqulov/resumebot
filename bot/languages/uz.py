words = {

    'home_menu': "Bosh sahifaga o'tish",

    'start_msg': "📲👇 Kerakli bolgan bo'limni tanlang",
    'phone': "Telefon raqamingizni jo'nating:",
    'edit_language': "🔄 Tilni o'zgartirish",
    'send_phone': "📱 Raqamimni jo'natish",
    "select_language": "Tilni tanlang 👇",
    'select_lang': "🇺🇿 🏪  Iltimos, tilni talang 👇",
    'home_button': "🏠️ Bosh sahifa",
    'back': '⏪ Orqaga',
    'lang': 'uz',

    'jobs': '📝 Bo\'sh ish o\'rinlari',
    'social_media': "🌏 Ijtimoiy tarmoqlar",
    'about_us': "📑 Biz haqimizda",

    'instagram': "💻 Instagram",
    'facebook': "📱 Facebook",
    'grandpharm_website': "🖥 Grandpharm veb-sayti",

    'about_text': "Biz haqimizda batafsil o'qish 📖 👇",

    's_media': "Ijtimoy tarmoqlar orqali malumot olish 👇",
    'information_about_us': "📑 Biz haqimizda ma'lumot",
    'jobs_vak': "Iltimos, bir nechta savollarga javob bering!",
    'blank_fill': "⌨️ Jarayonni boshlash",

    'instagram_link': "Instagram sahifasiga o'tish",
    'facebook_link': "Facebook sahifasiga o'tish",
    'grandpharm_link': "Grandpharm sahifasiga o'tish",

    'f_l_name': "Ism va familyangizni kiriting?",
    'birthday': "Tug'ulgan kuningizni kiriting?",
    'about_vacancy_network': "Vakansiyani qaysi bo'lim orqali bildingiz?",
    'company_work_friend': "Sizning kampanyada ishlaydigan do'stlaringiz bormi?",
    'job_interested': "Sizni qaysi vakansiya qiziqtiradi?",
    'work_schedule': "Sizning afzal ko'rgan ish jadvalingiz qanday?",
    'metro_station': "Eng yaqin metro bekati?",
    'your_education': "Sizning ma'lumotingiz qanday?",
    'specialty': "Sizning mutahasisligingiz?",
    'level_russian_language': "Rus tilini bilish darajangiz qanday?",
    'pupil_student': "Siz o'quvchi yoki talabamisiz?",
    'your_work_experience': "Sizning ish tajribangiz qanday?",
    'last_job': "Ohirgi ish joyingiz?",
    'minimum_wage': "Eng kam ish haqi?",
    'image': "O'z rasmingizni jo'nating?",
    'address': "O'z adresingizni to'liq kiriting?",

    'result': "Yakuni natija 👆",

    'information_about': "Biz haqimizda to'liq ma'lumotni bilish uchun \nShu link ustiga bosing",

    #KAK vakansi
    'network': "📲 Internet orqali",
    'rabotauz': "💻 Rabota.uz",
    'elon': "📜 Elon orqali",
    'friends': "👦/👦 Do'stlarim orqali",

    # work_ friend
    'yes': "👍 Ha",
    'no': "👎 Yo'q",

    # job_ interested
    'menejer': "🧑‍💻/👩‍💻 Dorixona Menejeri",
    'pharmacist': "👨‍⚕️/👩‍⚕️Farmatseft",
    'stajor': "👨‍🏫/👩‍🏫 Amalyotchi",

    # work time
    'morning': "08:00-16:00",
    'lunch': "12:00-20:00",
    'evening': "14:00-22:00",

    # your_education
    'high': "Oliy",
    'mediumspecial': "O'rta maxsus",

    # level russian language
    'elementary': "Boshlang'ich",
    'middle': "O'rta",
    'senior': "O'ziniki",

    # work_experience
    'no_work': "Ish tajribasi yo'q",
    '1-year': "1 yil",
    '2-5-year': "2-5 yil",
    '5-year+': "5 yildan yuqori",

    # finish
    'finish': "Barcha malumotlar to'g'rimi",
    'finish_yes': "Jarayon muofaqqiyatli yakunlandi. Raxmat",
    'finish_no': "Jarayonda xatolik yuz berdi. Qaytatdan urinib ko'ring",

    # result
    'name': "Ism Familyangiz: ",
    'phone_number': "Telefon raqamingiz: ",
    'specialty_user': "Mutahasisligingiz: ",
    'address_user': "Manzilingiz: ",
    'user_work_example': "Ish tajribangiz: ",

    'stiker': "👆👆"

}
