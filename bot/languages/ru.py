words = {

    'home_menu': "Перейти на главную страницу",

    'lang': 'ru',
    'phone': 'Отправьте свой номер телефона:',
    'select_lang': "🇷🇺 🏪 Пожалуйста, выберите язык 👇",
    'current_location': '📍 Мое местоположение',
    'home_button': "🏠️ Главное меню",
    'back': '⏪ Назад',
    'start_msg': " 💊 Введите название лекарства",
    'edit_language': "🔄 Изменить язык",
    "select_language": "Выберите язык 👇",
    'add_comment': '✍ Оставить отзыв',
    'search_drug': "🔍 Начать поиск",
    'send_phone': "📱 Отправить номер телефона",

    'about_us': "📑 О нас",
    'jobs': '📝 Вакансии',
    'social_media': "🌏 Социальные сети",

    'instagram': "💻 Instagram",
    'facebook': "📱 Facebook",
    'grandpharm_website': "🖥 Сайт grandpharm",

    'about_text': "Узнайте больше о нас 📖👇",

    's_media': "Получайте информацию через социальные сети 👇",
    'information_about_us': "📑 Информация о нас",
    'jobs_vak': "Просим ответить Вас на несколько вопросов!",
    'blank_fill': "⌨️ Начать процесс",

    'instagram_link': "Перейти в Инстаграм",
    'facebook_link': "Перейти на страницу Facebook",
    'grandpharm_link': "Перейти на страницу Грандфарм",

    'f_l_name': "Введите свое имя и фамилию?",
    'birthday': "Введите свой день рождения?",
    'about_vacancy_network': "Как вы узнали о вакансии?",
    'company_work_friend': "Есть ли кто из ваших знакомых кто работает в нашей компании?",
    'job_interested': "Какая вакансия вас интересует?",
    'work_schedule': "Какая вакансия вас интересует?",
    'metro_station': "Ближайшая станция метро ?",
    'your_education': "Какое у Вас оброзование ?",
    'specialty': "Какая специальность?",
    'level_russian_language': "Уровень вашего русского языка?",
    'pupil_student': "Являетесь ли Вы учеником или студентом в настоящее время ?",
    'your_work_experience': "Какой у Вас опыт работы?",
    'last_job': "Последнее место работы: Период, название организации, должность?",
    'minimum_wage': "Минимальный уровень зарплаты ?",
    'image': "Введите фотографию ",
    'address': "Введите свой адрес проживания",

    'result': "Конечный результат 👆",

    'information_about': "Для получения дополнительной информации о нас \ nНажмите на эту ссылку",

    # KAK vakansi
    'network': "📲 Соц.сети",
    'rabotauz': "💻 Rabota.uz",
    'elon': "📜 Обявление в аптеке",
    'friends': "👦/👦 Через знакомых",

    # work_ friend
    'yes': "👍 Да",
    'no': "👎 Нет",

    # job_ interested
    'menejer': "🧑‍💻/👩‍💻 Менеджер аптеки",
    'pharmacist': "👨‍⚕️/👩‍⚕️Фармацевт",
    'stajor': "👨‍🏫/👩‍🏫 Стажировка",

    # work time
    'morning': "08:00-16:00",
    'lunch': "12:00-20:00",
    'evening': "14:00-22:00",

    # your_education
    'high': "Высоко",
    'mediumspecial': "Средний специальный",

    # level russian language
    'elementary': "Начальный",
    'middle': "Средний",
    'senior': "Своюодный",

    # work_experience
    'no_work': "Нет опыта работы",
    '1-year': "до 1 года",
    '2-5-year': "от 2-5 лет",
    '5-year+': "более 5 лет",

    # finish
    'finish': "Все данные верны",
    'finish_yes': "Процесс завершился успешно. Спасибо",
    'finish_no': "В процессе произошла ошибка. Пожалуйста, попробуйте еще раз",

    'name': "Назовите свою семью: ",
    'phone_number': "Ваш номер телефона: ",
    'specialty_user': "Ваша специализация: ",
    'address_user': "Ваш адрес: ",
    'user_work_example': "Ваш опыт работы: ",

    'stiker': "👆👆"

}
