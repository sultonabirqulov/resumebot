from django.contrib import admin
from .models import Activity, Resume
# Register your models here.

admin.site.register(Activity)
admin.site.register(Resume)
