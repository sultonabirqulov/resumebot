from django.db import models
from account.models import User


class Activity(models.Model):
    chat_id = models.IntegerField()
    type = models.CharField(max_length=100, null=True, blank=True)
    detail = models.TextField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)


class Resume(models.Model):
    user = models.ForeignKey(User, blank=True, null=True, on_delete=models.DO_NOTHING)
    phone = models.CharField(max_length=255, null=True, blank=True)
    first_last_name = models.CharField(max_length=255, null=True, blank=True)
    birthday = models.CharField(max_length=255, null=True, blank=True)
    about_vacancy_network = models.CharField(max_length=255, null=True, blank=True)
    company_work_friend = models.CharField(max_length=255, null=True, blank=True)
    job_interested = models.CharField(max_length=255, null=True, blank=True)
    work_schedule = models.CharField(max_length=255, null=True, blank=True)
    metro_station = models.CharField(max_length=255, null=True, blank=True)
    your_education = models.CharField(max_length=255, null=True, blank=True)
    specialty = models.CharField(max_length=255, null=True, blank=True)
    level_russian_language = models.CharField(max_length=255, null=True, blank=True)
    pupil_student = models.CharField(max_length=255, null=True, blank=True)
    your_work_experience = models.CharField(max_length=255, null=True, blank=True)
    last_job = models.CharField(max_length=255, null=True, blank=True)
    minimum_wage = models.CharField(max_length=255, null=True, blank=True)
    image = models.ImageField(upload_to='static/images/', null=True, blank=True)
    address = models.CharField(max_length=255, null=True, blank=True)

    def __str__(self):
        return self.user.username