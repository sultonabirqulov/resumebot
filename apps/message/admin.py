from django.contrib import admin
from .models import Message, MessageSendLog
# Register your models here.
from account.models import User
from newbot.settings import TOKEN
from newbot import settings
import requests
import datetime


class MessageAdmin(admin.ModelAdmin):
    list_display = ['bot', 'title']
    list_filter = ['bot']
    search_fields = ['bot', 'title']

    # def save():
    #     time = datetime.datetime.now()
    #     users = User.objects.all()
    #     message = Message.objects.get(id=1)
    #     start_time = message.start_date
    #     for user in users:
    #         chat_id = str(user.chat_id)
    #
    #         token = settings.TOKEN
    #         text = "salom"
    #         url_req = "https://api.telegram.org/bot" + token + '/sendMessage' + '?chat_id=' + chat_id + '&text=' + text
    #         results = requests.get(url_req)
    #         print(results)
    #
    # save()


admin.site.register(Message)
admin.site.register(MessageSendLog)