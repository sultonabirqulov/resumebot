from django.db import models
from apps.telegram_bot.models import TelegramBot
from apps.telegram_bot.models import TelegramBot
# Create your models here.


MESSAGE_STATUS = (
    ('success', 'Success'),
    ('error', 'Error')
)


class Message(models.Model):
    bot = models.ForeignKey(TelegramBot, null=True, blank=True, on_delete=models.DO_NOTHING)
    title = models.CharField(max_length=255, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    image = models.ImageField(upload_to='static/message/images/', null=True, blank=True)
    start_date = models.DateTimeField(null=True)

    def __str__(self):
        return self.title


class MessageSendLog(models.Model):
    message = models.ForeignKey(Message, null=True, blank=True, on_delete=models.DO_NOTHING)
    chat_id = models.IntegerField(default=0)
    send_date = models.DateTimeField(auto_created=True)
    status = models.CharField(choices=MESSAGE_STATUS, max_length=255, null=True, blank=True)

    def __str__(self):
        return self.message.title